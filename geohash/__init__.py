from geohashrs import geohash_encode, geohash_decode

from flask import Flask,request

app = Flask(__name__)

@app.route('/')
def index():
    return 'Geohas API'

@app.route('/ws/encode/<float:latitude>/<float:longitude>/<int:level>')
def encode(latitude, longitude, level):
    return geohash_encode(latitude, longitude, level)

@app.route('/ws/decode/<string:geohash>')
def decode(geohash):
    result =geohash_decode(geohash) 
    print(result)
    return  { 
        "lat": result[0],
        "long": result[1]
    }