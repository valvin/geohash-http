FROM python:3

COPY . /app
RUN python3 -m pip install -r /app/requirements.txt
ENV FLASK_APP geohash
WORKDIR /app
EXPOSE 5000
CMD [ "flask", "run", "--host=0.0.0.0"]